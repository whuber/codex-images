---
title: "Myc analysis"
author: "Wolfgang Huber"
date: "`r date()`"
format:
  html:
    embed-resources: true
    page-layout: full
    toc: true
    css: wh.css
highlight-style: tango
---

# Load packages

```{r}
#| label: loadpkgs
#| message: false
#| warning: false
library("dplyr")
library("ggplot2")
library("ggbeeswarm")
```

# Data

- #(T cells) => T CD3
- #(all cells) => Total
- patient ID => sample_id
- MYC status  => MYC-BAP

```{r}
#| label: readdata
d = readr::read_csv("myc_table.csv")
colnames(d)
stopifnot(all(rowSums(d[,3:15]) - d$Total == 0))
table(table(d$core))
table(table(d$sample_id))
```

# Sum replicates with same `sample_id`

```{r}
#| label: patsum
p = group_by(d, sample_id) |>
  summarize(
    T   = sum(`T CD3`),
    tot = sum(`Total`),
    `MYC-BAP` = mean(`MYC-BAP`, na.rm = TRUE)
  )
stopifnot(all(p$`MYC-BAP` %in% c(0, 1, NaN)))
table(p$`MYC-BAP`)
p = filter(p, !is.nan(`MYC-BAP`)) |> 
    mutate(`MYC-BAP` = factor(c("wt", "mut")[`MYC-BAP` + 1], levels = c("wt", "mut")))
```

```{r}
#| label: plot
ggplot(p, aes(x = `MYC-BAP`, y = T/tot, color = `MYC-BAP`)) + 
  geom_beeswarm(alpha = 0.7, size = 2) + 
  theme_minimal() +
  scale_color_brewer(palette = "Set2")
```

```{r}
#| label: test
t.test(T/tot ~ `MYC-BAP`, data = p)
```


# Session Info

```{r}
#| label: sessioninfo
Sys.time()
devtools::session_info()
```



