# Data from EMBL file server to local
rsync -vrclpt datatransfer.embl.de:/g/huber/projects/CITEseq/CODEX/BNHL_TMA/cropped_automated_3k /Users/whuber/Data/
rsync -vrclpt datatransfer.embl.de:/g/huber/projects/CITEseq/CODEX/BNHL_TMA/cropped_metadata_3k /Users/whuber/Data/


# Local to Webpage
rsync -vrclpt test-periodogram.html test-periodogram_files channel-correlations.html channel-correlations_files cc-all.Rdata datatransfer.embl.de:/g/huber/www-huber/users/whuber/230724-ErinCODEXImagesPeriodogram
